#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json

from . import app_settings as settings
from django.core.serializers.json import DjangoJSONEncoder

handler = {
    "disconnect": [],
    "connect": [],
}

requests.packages.urllib3.disable_warnings()

def generate_socket_url(idx, url):
    return settings.SOCKETIO_INT_URL.split(" ")[idx] + url

def post_to_socket(url, data, response_type=None):
    headers = {"content-type": "application/json"}
    data["appid"] = settings.SOCKET_APP_ID
    for i in range(len(settings.SOCKETIO_INT_URL.split(" "))):
        try:
            socketurl = generate_socket_url(i,url)
            r = requests.post(socketurl, data=json.dumps(data, cls=DjangoJSONEncoder), headers=headers, verify=False)
        except Exception as e:
            print("Could not connect to SocketIO Server. Error follows.")
            print(e)
    if response_type == "json":
        return r.json()

def get_sockets_of_rooms(rooms):
    return post_to_socket("/get_sockets_of_rooms/", {"rooms": rooms}, "json")

def send_join(room, sessionid):
    post_to_socket("/joined/", {"room": room, "sessionid": sessionid})

def send_leave(room, sessionid):
    post_to_socket("/left/", {"room": room, "sessionid": sessionid})

def send(message, sessionid):
    post_to_socket("/send/", {"message": message, "sessionid": sessionid})

def send_to(message, room, sessionid):
    post_to_socket("/send_to/", {"message": message, "sessionid": sessionid, "room": room})

def emit(message):
    post_to_socket("/emit/", {"message": message})

def emit_to(message, room):
    post_to_socket("/emit_to/", {"message": message, "room": room})

def on(name, func):
    if name not in handler:
        handler[name] = []
    handler[name].append(func)

def fire(name, sessionid, data, request):
    if name in handler:
        for func in handler[name]:
            func(sessionid, data, request)


def on_join(room=""):
    event = "join:" + room

    def method_wrapper(func):
        if event not in handler:
            handler[event] = []
        handler[event].append(func)
        return func
    return method_wrapper

def on_leave(room=""):
    event = "leave:" + room
    def method_wrapper(func):
        if event not in handler:
            handler[event] = []
        handler[event].append(func)
        return func
    return method_wrapper

def on_connect(func):
    handler["connect"].append(func)
    return func

def on_disconnect(func):
    handler["disconnect"].append(func)
    return func
