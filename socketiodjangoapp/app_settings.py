#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings

SOCKET_APP_ID = getattr(settings, "SOCKET_APP_ID", "none")

SOCKETIO_EXT_URL = getattr(settings, "SOCKETIO_EXT_URL", "http://localhost:4000")
SOCKETIO_INT_URL = getattr(settings, "SOCKETIO_INT_URL", "http://localhost:4000")
SOCKETIO_PATH_PREFIX = getattr(settings, "SOCKETIO_PATH_PREFIX", "socket/")
SOCKETIO_WEB_URL = getattr(settings, "SOCKETIO_WEB_URL", "http://localhost:8000")
