#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^join/$', views.join),
    url(r'^leave/$', views.leave),
    url(r'^connect/$', views.connect),
    url(r'^disconnect/$', views.disconnect)
]
