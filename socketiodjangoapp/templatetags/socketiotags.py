#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json, random
from django import template
from django.utils.safestring import mark_safe
from socketiodjangoapp import app_settings as settings


register = template.Library()

@register.simple_tag
def js_io_vars():
    ext_url = settings.SOCKETIO_EXT_URL.split(" ")
    length = len(ext_url)
    if length > 1:
        idx = random.randint(0,length-1)
    else:
        idx = 0
    obj = json.dumps({
        "url": ext_url[idx],
        "path_prefix": settings.SOCKETIO_PATH_PREFIX,
        "web_url": settings.SOCKETIO_WEB_URL
    })
    return mark_safe("<script>window.SOCKETIO_CONF=JSON.parse('" + obj + "');</script><script src='" + ext_url[idx] + "/socket.io/socket.io.js'></script>")
