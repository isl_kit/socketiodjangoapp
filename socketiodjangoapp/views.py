#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import re

from django.http import JsonResponse, HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .api import fire, send_join, send_leave
from .conf import io_auth


def authenticate_join(request, room):
    authenticated = True
    for room_re, auth_func in io_auth.auth_data.items():
        if re.match(room_re, room):
            authenticated = authenticated and auth_func(request, room)
    return authenticated


@require_POST
def join(request):
    data = json.loads(request.body)
    try:
        sessionid = data["sessionid"]
        room = data["room"]
        authenticated = authenticate_join(request, room)
        if authenticated:
            send_join(room, sessionid)
            fire("join:" + room, sessionid, {}, request)
            fire("join:", sessionid, {"room": room}, request)
        return JsonResponse({"authenticated": authenticated})
    except Exception as e:
        print(traceback.format_exc())
        return HttpResponseServerError(str(e))


@require_POST
def leave(request):
    data = json.loads(request.body)
    try:
        sessionid = data["sessionid"]
        room = data["room"]
        send_leave(room, sessionid)
        fire("leave:" + room, sessionid, {}, request)
        fire("leave:", sessionid, {"room": room}, request)
        return JsonResponse({})
    except Exception as e:
        return HttpResponseServerError(str(e))


@csrf_exempt
@require_POST
def connect(request):
    data = json.loads(request.body)
    try:
        sessionid = data["sessionid"]
        send_join("", sessionid)
        fire("connect", sessionid, {}, request)
        return HttpResponse("Everything worked :)")
    except Exception as e:
        return HttpResponseServerError(str(e))


@csrf_exempt
@require_POST
def disconnect(request):
    data = json.loads(request.body)
    try:
        sessionid = data["sessionid"]
        fire("disconnect", sessionid, {}, request)
        return HttpResponse("Everything worked :)")
    except Exception as e:
        return HttpResponseServerError(str(e))
