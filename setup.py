from distutils.core import setup

setup(
    name="SocketioDjangoApp",
    version="1.0.0",
    author="Bastian Krüger",
    author_email="bastian.krueger@kit.edu",
    packages=["socketiodjangoapp", "socketiodjangoapp.templatetags"],
    url="https://bitbucket.org/isl_kit/socketiodjangoapp.git",
    license="LICENSE.txt",
    description="A django app for communication between a Socket.IO server and a webserver.",
    long_description=open("README.md").read(),
    install_requires=[
        "requests>=2.6.0,<3.0.0"
    ],
    zip_safe=False,
)
