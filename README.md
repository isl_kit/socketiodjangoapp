# Socket.IO Django App

This is a django app, that is intended to be used in combination with [Socket.IO server](https://bitbucket.org/isl_kit/socketioserver.git). It provides functions to access the API of the Socket.IO server as well as an own API, to be accessed by the client or the Socket.IO server.

## Setup

### Install app

    pip install git+https://bitbucket.org/isl_kit/socketiodjangoapp.git

### Include App

Include this app into the `INSTALLED_APPS` list of your django configuration:

    INSTALLED_APPS = [
        # ...
        'socketiodjangoapp',
        # ...
    ]

### Include Urls

Make the API of this app available, by including the routes to the main `urls.py` file:

    urlpatterns = [
        url(r'^' + settings.SOCKETIO_PATH_PREFIX, include('socketioapi.urls')),
    ]

### Include JavaScript files

At the top of the main `index.html` file, include `socketiotags`:

    {% load socketiotags %}
    <!DOCTYPE html>
    <html lang="en">
        <!-- ... -->
    </html>

Then, inside the `<head>` section, set JavaScript variables:

    <head>
    {% js_io_vars %}
    </head>

This includes the Socket.IO JavaScript client, as well the configuration of this app, stored in `window.SOCKETIO_CONF` (see [Config](#config)).


## Socket.IO Server API calls

This app provides several functions to call according urls of the Socket.IO server. For more information of what each of those calls do on the Socket.IO server, check the repository [https://bitbucket.org/isl_kit/socketioserver](https://bitbucket.org/isl_kit/socketioserver).

### send_join

Sends a POST request to `/joined/` on the Socket.IO server API.

    from socketiodjangoapp.api import send_join

    send_join(room, sessionid)

#### Params:

- room: the room to which the client should be added
- sessionid: the ID of the Socket.IO session

### send_leave

Sends a POST request to `/left/` on the Socket.IO server API.

    from socketiodjangoapp.api import send_leave

    send_leave(room, sessionid)

#### Params:

- room: the room from which the client should be removed
- sessionid: the ID of the Socket.IO session


### send

Sends a POST request to `/send/` on the Socket.IO server API.

    from socketiodjangoapp.api import send

    send(message, sessionid)

#### Params:

- message: the message to send to the client
- sessionid: the ID of the Socket.IO session

### send_to

Sends a POST request to `/send_to/` on the Socket.IO server API.

    from socketiodjangoapp.api import send

    send_to(message, room, sessionid)

#### Params:

- message: the message to send to the client
- room: the room in which the client is supposed to be
- sessionid: the ID of the Socket.IO session

### emit

Sends a POST request to `/emit/` on the Socket.IO server API.

    from socketiodjangoapp.api import emit

    emit(message)

#### Params:

- message: the message to send to the client

### emit_to

Sends a POST request to `/emit_to/` on the Socket.IO server API.

    from socketiodjangoapp.api import emit_to

    emit_to(message, room)

#### Params:

- message: the message to send to the client
- room: the room to which the message should be broadcasted


## API

The API of this app serves two functions:

- communication between the client and the web server
- communication between the web server and the Socket.IO server

Therefore, some urls have to be called from the client and some from the Socket.IO server.

### POST: /join/

Intended to be called from the client (browser), when the client wishes to join a certain room. If the client has the necessary permissions, a request to the Socket.IO server's `/joined/` endpoint will be sent, to add the user to the specified room.

    body: {
        sessionid: '48561838583',
        room: 'roomXY'
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- room: the name of the room, the client whishes to join

### POST: /leave/

Intended to be called from the client (browser), when the client wishes to leave a certain room. No permissions necessary. A request to the Socket.IO server's `/left/` endpoint will be sent, to remove the user from the specified room.

    body: {
        sessionid: '48561838583',
        room: 'roomXY'
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- room: the name of the room, the client should be removed from

### POST: /connect/

Intended to be called from the Socket.IO server, whenever a new client connects. This enables the web server to implement event handlers and execute any kind of logic on each new connection.

    body: {
        sessionid: '48561838583'
    }

#### Params:

- sessionid: the ID of the Socket.IO session

### POST: /disconnect/

Intended to be called from the Socket.IO server, whenever a client disconnects. This enables the web server to implement event handlers and execute any kind of logic on each disconnection.

    body: {
        sessionid: '48561838583'
    }

#### Params:

- sessionid: the ID of the Socket.IO session


## Permissions

To check if a client has the permission to join a certain room, custom permission checks can be implemented. Only if all checks succeed will the request be forwarded to the Socket.IO server. These permission checks have to be defined in a single location in the application, in the following way:

    from socketiodjangoapp.conf import io_auth

    def check1(request, room):
        # permission check here
        return True

    def check2(request, room):
        # permission check here
        return False

    io_auth.set_data({
        'room1': check1,
        'room2': check2,
        # ...
    })

The keys are regular expressions that matches a room name. `check1` and `check2` are functions that take two arguments (the django request object and the room name) and return either `True` or `False`, depending on whether the client has the necessary permissions or not.

## Events

### on_join

This event fill be triggered any time a client joins a certain room. It will only be triggered if all permission checks were successfull.

    from socketiodjangoapp.api import on_join

    @on_join()
    def on_join_handler(sessionid, data, request):
        pass


#### Params:

- sessionid: the ID of the Socket.IO session
- data: a dict with the following items:
    - room: name of the room
- request: django request object

### on_leave

This event will be triggered any time a client leaves a certain room.

    from socketiodjangoapp.api import on_leave

    @on_leave()
    def on_leave_handler(sessionid, data, request):
        pass


#### Params:

- sessionid: the ID of the Socket.IO session
- data: a dict with the following items:
    - room: name of the room
- request: django request object

### on_connect

This event will be triggered any time a new client connect to the Socket.IO server.

    from socketiodjangoapp.api import on_connect

    @on_connect()
    def on_connect_handler(sessionid, data, request):
        pass


#### Params:

- sessionid: the ID of the Socket.IO session
- data: an empty dict
- request: django request object

### on_disconnect

This event will be triggered any time a client disconnects from the Socket.IO server.

    from socketiodjangoapp.api import on_disconnect

    @on_disconnect()
    def on_disconnect_handler(sessionid, data, request):
        pass


#### Params:

- sessionid: the ID of the Socket.IO session
- data: an empty dict
- request: django request object

## Config

### Django Config

The following shows all available django configuration parameters for this app, along with their default values.

#### SOCKET_APP_ID

This ID of this app:

    SOCKET_APP_ID=none

This ID is important, if multiple applications connect to the same Socket.IO server. In that case, every app has to chose a unique app ID.

#### SOCKETIO_PATH_PREFIX

The path under which, the API of this app will be reachable:

    SOCKETIO_PATH_PREFIX=socket/

#### SOCKETIO_EXT_URL

This is the url under which the Socket.IO server will reachable from a client (browser).

    SOCKETIO_EXT_URL=http://localhost:4000

#### SOCKETIO_INT_URL

This is the url under which the Socket.IO server will reachable from the web server.

    SOCKETIO_INT_URL=http://localhost:4000

#### SOCKETIO_WEB_URL

This is the url under which the web server will be reachable from the Socket.IO server.

    SOCKETIO_WEB_URL=http://localhost:8000

### JavaScript Config

This app also includes a JavaScript object in the main `index.html` that contains some of the configuration, for the further usage within the client (browser):

    window.SOCKETIO_CONF = {
        url        : SOCKETIO_EXT_URL,
        path_prefix: SOCKETIO_PATH_PREFIX,
        web_url    : SOCKETIO_WEB_URL
    };
